﻿using UnityEngine;

public class AlphaPotatoManager : MonoBehaviour
{
    [HideInInspector]
    public GameState gameState = GameState.NONE;

    [HideInInspector]
    public Vector3 initialPosition, initialRotation;

    public virtual void Awake()
    {
        initialPosition = transform.position;
        initialRotation = transform.eulerAngles;
    }

    public virtual void Reset()
    {
        transform.position = initialPosition;
        transform.eulerAngles = initialRotation;
    }

    public virtual void OnEnable()
    {
        GameManager.OnGameOver += OnGameOver;
    }

    public virtual void OnDisable()
    {
        GameManager.OnGameOver -= OnGameOver;
    }

    public virtual void OnGameOver(bool result)
    {
        gameState = GameState.GAMEOVER;
    }
}

public enum GameState
{
    NONE,
    PAUSE,
    PLAY,
    GAMEOVER
}