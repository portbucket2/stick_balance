﻿using UnityEngine;

public class BreakDetection : MonoBehaviour
{
    void OnJointBreak(float breakForce)
    {
        Debug.Log("A joint has just been broken!, force: " + breakForce);
        if (GameManager.instance.gameState.Equals(GameState.PLAY))
            GameManager.instance.GameOver(false);
    }
}
