﻿using UnityEngine;

public class CameraFollow : AlphaPotatoManager
{
    public Transform target, lookAt;
    public Vector3 initialOffset, gameplayOffset;
    public GameObject gameOverEffect;

    public override void OnGameOver(bool result)
    {
        base.OnGameOver(result);

        gameOverEffect.SetActive(result);
    }

    // Update is called once per frame
    void Update()
    {
        if (!target)
            return;

        transform.position = Vector3.Lerp(transform.position, target.position + gameplayOffset, 0.5f);
        transform.LookAt(lookAt);
        //transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z);


        //Vector3 lTargetDir = lookAt.position - transform.position;
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lTargetDir), 0.3f);
    }
}
