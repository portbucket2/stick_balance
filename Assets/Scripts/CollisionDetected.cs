﻿using UnityEngine;

public class CollisionDetected : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(GameManager.instance.gameState.Equals(GameState.PLAY))
            GameManager.instance.GameOver(false);
    }
}
