﻿using System;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public static event Action<bool> OnGameOver;

    public StickInputController stickInputController;
    public LevelAsset levelAsset;
    public GameState gameState;
    public float animationTime = 1;
    public float gamePlayTime = 15;
    public Image progressClock;
    public GameObject gameOverPanel, win, loose, nextButton, title, timerCicle;

    [HideInInspector]
    public int currentLevelIndex;
    [HideInInspector]
    public float totalProgressInHand, totalProgressToHave;
    const string currentLevel = "CurrentLevel";

    private void Awake()
    {
        instance = this;
        DOTween.Init();
        DOTween.Clear();
        SetupThisLevel();
    }

    void SetupThisLevel()
    {
        //PlayerPrefs.DeleteAll();

        currentLevelIndex = PlayerPrefs.GetInt(currentLevel, 0);
        GenarateLevelsEnvironment();
        title.SetActive(true);
        timerCicle.SetActive(false);
        nextButton.SetActive(false);
        gameOverPanel.SetActive(false);
    }

    private void GenarateLevelsEnvironment()
    {
        stickInputController.SetupLevel(levelAsset.levels[currentLevelIndex]);
    }

    public void LoadNextLevel()
    {
        Application.LoadLevel(0);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            title.SetActive(false);
            timerCicle.SetActive(true);
        }

        if (Input.GetMouseButtonDown(1))
            GameOver(true);
    }

    public void UpdateProgress(float time)
    {
        progressClock.fillAmount = time / gamePlayTime;

        if(time >= gamePlayTime)
        {
            GameOver(true);
        }
    }

    public void GameOver(bool result)
    {
        Debug.Log("Game Over : " + result);
        OnGameOver(result);

        gameOverPanel.SetActive(true);
        win.SetActive(result);
        loose.SetActive(!result);
        nextButton.SetActive(true);
        gameState = GameState.GAMEOVER;

        if (result)
        {
            // Ready PlayerPref for next level to Initialize.
            PlayerPrefs.SetInt(currentLevel, (currentLevelIndex + 1) % levelAsset.levels.Length);

        }
    }

    public void EnablePlaying()
    {
        gameState = GameState.PLAY;
    }

}
