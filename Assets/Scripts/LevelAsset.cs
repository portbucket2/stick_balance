﻿using UnityEngine;

[CreateAssetMenu(menuName ="LevelAsset/Create", fileName ="LevelAssets")]
public class LevelAsset : ScriptableObject
{
    public Level[] levels;
}

[System.Serializable]
public class Level
{
    public GameObject prefab;
    [Range(0, 1)]
    public float drag;
    public float moveSensitivity = 0.1f;

}