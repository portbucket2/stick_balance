﻿using UnityEngine;
using System.Collections.Generic;

public class Pooler : MonoBehaviour
{
    Dictionary<string, Queue<GameObject>> poolDictionary = new Dictionary<string, Queue<GameObject>>();

    public GameObject Instantiate(GameObject prefabObj) {

        // Finally this object will be return
        GameObject obj;

        // Make sure type is not null
        CheckTypeExist(prefabObj.tag);

        // Make sure there is atleast one object to return
        if (poolDictionary[prefabObj.tag].Count == 0) {

            obj = Instantiate(prefabObj);
            obj.SetActive(false);
            obj.transform.parent = transform;
            poolDictionary[prefabObj.tag].Enqueue(obj);
        }

        // Finally pool a object & return;
        obj = poolDictionary[prefabObj.tag].Dequeue();
        obj.transform.parent = null;
        return obj;
    }

    public void Destroy(GameObject obj) {

        if(obj == null) {

            Debug.Log("A null object can't be stored in pooler.");
            return;
        }

        // Make sure type is not null
        CheckTypeExist(obj.tag);

        obj.SetActive(false);
        obj.transform.parent = transform;
        poolDictionary[obj.tag].Enqueue(obj);
    }

    void CheckTypeExist(string tagName)
    {
        // If this prefab type is not in dictionary,
        // then make a type by it's tag name.
        if (!poolDictionary.ContainsKey(tagName))
            poolDictionary[tagName] = new Queue<GameObject>();
    }

}
