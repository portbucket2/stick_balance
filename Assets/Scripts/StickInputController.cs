﻿using UnityEngine;

public class StickInputController : AlphaPotatoManager
{
    public Animator anim;
    public Transform baseHolder, sticeBottomPoint, world;
    public float animationOffset = 0f;

    Vector3 lastMousePosition;
    bool play, gameStarted = false;

    float inputSensitivity;
    float gameStartedTime = 0;
    Transform stickTopPoint;
    CharacterJoint joint;
    public void SetupLevel(Level level)
    {
        GameObject go = Instantiate(level.prefab, baseHolder);
        stickTopPoint = go.transform.GetChild(0);
        stickTopPoint.parent = null;
        stickTopPoint.gameObject.AddComponent<CollisionDetected>();
        joint = sticeBottomPoint.gameObject.AddComponent<CharacterJoint>();
        Rigidbody topStickRig = stickTopPoint.GetComponent<Rigidbody>();
        topStickRig.drag = level.drag;
        joint.connectedBody = topStickRig;
        joint.lowTwistLimit = new SoftJointLimit {

            limit = -177
        };
        joint.highTwistLimit = new SoftJointLimit
        {
            limit = 177
        };
        joint.swing1Limit = new SoftJointLimit
        {
            limit = -177
        };
        joint.swing2Limit = new SoftJointLimit
        {
            limit = 177
        };
        joint.projectionAngle = 180;
        joint.breakForce = 150;
        joint.breakTorque = Mathf.Infinity;

        inputSensitivity = level.moveSensitivity;

        Destroy(go);
    }

    public override void OnGameOver(bool result)
    {
        base.OnGameOver(result);
        joint.connectedBody = null;
        if (result)
        {
            stickTopPoint.GetComponent<Rigidbody>().isKinematic = result;
            anim.SetTrigger("GameOverWin");
        }
        else
            anim.SetTrigger("GameOverLose");

        stickTopPoint.gameObject.SetActive(!result);

    }

    public void OnMouseDown()
    {
        if (!gameStarted)
        {
            gameStarted = true;
            gameStartedTime = Time.time;
            GameManager.instance.gameState = GameState.PLAY;
            float range = 0.05f;
            stickTopPoint.position = new Vector3(range, stickTopPoint.position.y, range);
        }
        play = true;
        //stickTop.localPosition = new Vector3(0.1f, 0, 0);
        lastMousePosition = Input.mousePosition;
    }

    public void OnMouseUp()
    {
        play = false;
    }

    public void Update()
    {
        anim.SetFloat("FaceExpression", sticeBottomPoint.eulerAngles.magnitude * Time.deltaTime, 0.1f, Time.deltaTime);
        if (gameStarted && GameManager.instance.gameState.Equals(GameState.PLAY))
        {
            GameManager.instance.UpdateProgress(Time.time - gameStartedTime);
        }

        if (!play || !GameManager.instance.gameState.Equals(GameState.PLAY))
        {
            SimulateMovementAnimation(Vector3.zero);
            anim.SetFloat("SpeedMultiplyer", 1);
            return;
        }


        Vector3 mouseDelta = Input.mousePosition - lastMousePosition;
        mouseDelta = Time.deltaTime * inputSensitivity * mouseDelta;
        mouseDelta.z = mouseDelta.y;
        mouseDelta.y = 0;


        SimulatePhysics(mouseDelta);
        SimulateRotation(mouseDelta);
        SimulateMovementAnimation(-mouseDelta);
    }

    private void SimulateRotation(Vector3 mouseDelta)
    {
        sticeBottomPoint.eulerAngles += mouseDelta;

    }

    void SimulatePhysics(Vector3 mouseDelta)
    {
        baseHolder.position = Vector3.Lerp(baseHolder.position, baseHolder.position + mouseDelta, 0.3f);
        //world.eulerAngles += new Vector3(mouseDelta.z, 0, mouseDelta.x);
        world.Rotate(-mouseDelta.z, 0, mouseDelta.x, Space.World);
    }

    public void SimulateMovementAnimation(Vector3 mouseDelta)
    {        
        anim.SetFloat("SpeedMultiplyer", mouseDelta.magnitude * 5);
        mouseDelta.Normalize();
        anim.SetFloat("Horizontal", mouseDelta.x);
        anim.SetFloat("Vertical", mouseDelta.z);
    }
}
